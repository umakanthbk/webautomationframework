package generics;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utility.DriverHelper;

public abstract class BasePage 
{
	public WebDriver driver;
	
	public WebDriverWait wait;
	
	public DriverHelper driverhelper;
	
	public BasePage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
		wait=new WebDriverWait(driver, 30);
		driverhelper = new DriverHelper();
	}
	public void navigatetomypage(String url)
	{
		driver.get(url);
	}
	
}
