package generics;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import Utility.ScreenshotHelper;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest implements Auto_const
{
	public WebDriver driver;
	
	@BeforeMethod
	public void OpenAppl()
	{
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
//		driver.get("https://www.naukri.com/");
	}
	
	@AfterMethod
	public void CloseAppl(ITestResult result) throws IOException
	{
		String testMethodName = result.getMethod().getMethodName();
		System.out.println("method name:" + testMethodName);
		
		if(!result.isSuccess())
		{
		ScreenshotHelper screenshothelperobject = new ScreenshotHelper();
			screenshothelperobject.takeFailedTestScreenshot(driver,testMethodName);
		}
		driver.quit();
	}

}
