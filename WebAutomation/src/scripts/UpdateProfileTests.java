package scripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import generics.BaseTest;
import pom.HomepagePOM;
import pom.LoginPagePOM;
import pom.UpdateProfilePOM;

public class UpdateProfileTests extends BaseTest
{
	@Test
	public void verifyUpdateProfile() throws InterruptedException
	{
		System.out.println("verify test update profile");
		LoginPagePOM loginpageobject=new LoginPagePOM(driver);
		loginpageobject.Login("bkumakanth@gmail.com", "password@123");
		HomepagePOM homepageobject=new HomepagePOM(driver);
		homepageobject.clickUpdateProfileButton();
		UpdateProfilePOM updateProfilepageobject=new UpdateProfilePOM(driver);
		boolean haveIEnteredUpdateProfilepage=updateProfilepageobject.verifyUpdateProfilePage("Profile | Mynaukri");
		Assert.assertEquals(haveIEnteredUpdateProfilepage, true);
	}

}
