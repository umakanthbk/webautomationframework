package scripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import generics.BaseTest;
import pom.HomepagePOM;
import pom.LoginPagePOM;
import pom.SearchAppearencePOM;

public class SearchAppearenceTests extends BaseTest
{
	@Test
	public void verifySearchAppearence()
	{
		System.out.println("verify test search appearence");
		LoginPagePOM loginpageobject=new LoginPagePOM(driver);
		loginpageobject.Login("bkumakanth@gmail.com", "password@123");
		HomepagePOM homepageobject=new HomepagePOM(driver);
		homepageobject.clicksearchAppearencelink();
		SearchAppearencePOM seaarchappearenceobject=new SearchAppearencePOM(driver);
		boolean haveIEnteredSearchAppearecePage=seaarchappearenceobject.verifysearchAppearencelinkpage("Profile Action | Mynaukri");
		Assert.assertEquals(haveIEnteredSearchAppearecePage,true);
	}

}
