package scripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import generics.BaseTest;
import pom.HomepagePOM;
import pom.LoginPagePOM;
import pom.RecruiterActionsPOM;

public class RecruiterActionsTests extends BaseTest
{
	@Test
	public void verifyRecruiter() throws InterruptedException
	{
		System.out.println("verify test recruiter");
		LoginPagePOM loginpageobject=new LoginPagePOM(driver);
		loginpageobject.Login("bkumakanth@gmail.com", "password@123");
		HomepagePOM homepageobject=new HomepagePOM(driver);
		homepageobject.clickrecruiteractionslink();
		RecruiterActionsPOM recruiteractionsobject=new RecruiterActionsPOM(driver);
		boolean haveIEnteredRecruiterActionPage=recruiteractionsobject.verifyrecruiteractionslinkpage("Profile Action | Mynaukri");
		Assert.assertEquals(haveIEnteredRecruiterActionPage, true);
	}

}
