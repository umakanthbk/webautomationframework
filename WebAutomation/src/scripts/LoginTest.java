package scripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import generics.BaseTest;
import pom.HomepagePOM;
import pom.LoginPagePOM;

public class LoginTest extends BaseTest
{
	@Test
	public void VerifyLogin()
	{
		//Step 1-Verify Login
		LoginPagePOM loginpageobject=new LoginPagePOM(driver);
		loginpageobject.Login("bkumakanth@gmail.com","password@123");
		HomepagePOM homepageobject=new HomepagePOM(driver);
		boolean haveILoggedin=homepageobject.verifyUsernameText("B K Umakanth");
		Assert.assertEquals(haveILoggedin, true);
	}
	
	@Test
	public void verifyinvalidLogin()
	{
		System.out.println("executing test verify invalid login");
		LoginPagePOM loginpageobject=new LoginPagePOM(driver);
		loginpageobject.Login("fsfafd", "fsdafs");
		boolean haveErrorMsg=loginpageobject.verifyERRORmsg("Invalid details. Please check the Email ID - Password combination.");
		Assert.assertEquals(haveErrorMsg, true);
	}

}
