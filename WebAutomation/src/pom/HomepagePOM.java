package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import generics.BasePage;

public class HomepagePOM extends BasePage
{
	@FindBy(css=".user-name.roboto-bold-text")
	private WebElement usernamelabel;
	
	@FindBy(xpath="//div[contains(text(),'Recruiter Actions')]")
	private WebElement recruiteractionslink;
	
	@FindBy(css=".searchBlock.flexible.card")
	private WebElement searchAppearencelink;
	
	@FindBy(css=".btn.btn-block.btn-large.white-text")
	private WebElement updateProfileButton;
	
	@FindBy(xpath="//div[text()='Jobs' and @class='mTxt']")
	private WebElement jobsmodule;
	
	@FindBy(xpath="//div[text()='Recruiters' and @class='mTxt']")
	private WebElement recruitersmodule;
	
	@FindBy(xpath="//div[text()='Companies' and @class='mTxt']")
	private WebElement companiesmodule;
	
	@FindBy(xpath="//div[text()='Tools' and @class='mTxt']")
	private WebElement toolsmodule;
	
	@FindBy(xpath="//div[text()='Services' and @class='mTxt']")
	private WebElement servicesmodule;
	
	@FindBy(xpath="//div[text()='More' and @class='mTxt']")
	private WebElement moremodule;
	
	public HomepagePOM(WebDriver driver) 
	{
		super(driver);
	}

	public boolean verifyUsernameText(String username)
	{
//		WebDriverWait wait=new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(usernamelabel));
		wait.until(ExpectedConditions.textToBePresentInElement(usernamelabel, username));
		String actualText=usernamelabel.getText();
		if(actualText.equals(username))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public void clickrecruiteractionslink()
	{
		wait.until(ExpectedConditions.elementToBeClickable(recruiteractionslink));
		recruiteractionslink.click();
	}
	
	public void clicksearchAppearencelink()
	{
		wait.until(ExpectedConditions.elementToBeClickable(searchAppearencelink));
		searchAppearencelink.click();
	}
	
	public void clickUpdateProfileButton()
	{
		wait.until(ExpectedConditions.visibilityOf(updateProfileButton));
		updateProfileButton.click();
	}
	
	public void clickJobsModule()
	{
		jobsmodule.click();
	}
	
	public void clickRecruitersModule()
	{
		recruitersmodule.click();
	}
	
	public void clickCompaniesModule()
	{
		companiesmodule.click();
	}
	
	public void clickToolsModule()
	{
		toolsmodule.click();
	}
	
	public void clickServicesModule()
	{
		servicesmodule.click();
	}
	
	public void clickMoreModule()
	{
		moremodule.click();
	}
	
}
