package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import generics.BasePage;

public class RecruiterActionsPOM extends BasePage
{

	@FindBy(css=".page-heading")
	private WebElement profileperformance;
	
	public RecruiterActionsPOM(WebDriver driver) 
	{
		super(driver);
		
	}
		
	public boolean verifyrecruiteractionslinkpage(String expected)
	{
		wait.until(ExpectedConditions.titleContains(expected));
		String actual=driver.getTitle();
		if(actual.equals(expected))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
