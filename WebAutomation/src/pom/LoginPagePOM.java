package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import generics.BasePage;

public class LoginPagePOM extends BasePage
{

	@FindBy(xpath="//div[contains(text(),'Login')]")
	private WebElement loginButton;
	
	@FindBy(xpath="//input[@placeholder='Enter your active Email ID / Username']")
	private WebElement userNameTextBox;
	
	@FindBy(xpath="//input[@type='password']")
	private WebElement passwordtextbox;
	
	@FindBy(css=".btn-primary.loginButton")
	private WebElement Signinbutton;
	
	@FindBy(xpath="//div[contains(text(),'Invalid details')]")
	private WebElement ErrorMessageLabel;
	
	public LoginPagePOM(WebDriver driver) 
	{
		super(driver);
		
	}
	
	public void Login(String username, String password)
	{
		navigatetomypage("https://www.naukri.com/");
		driverhelper.CloseOtherBrowsers(driver);
//		driver.get("https://www.naukri.com/");
		loginButton.click();
//		driver.findElement(By.xpath("//div[contains(text(),'Login')]")).click();
//		WebDriverWait wait=new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(userNameTextBox));
//		System.out.println(driver.getPageSource());
		userNameTextBox.sendKeys(username);
		passwordtextbox.sendKeys(password);
		Signinbutton.click();
	}
	public boolean verifyERRORmsg(String expectedErrormsg)
	{
		wait.until(ExpectedConditions.visibilityOf(ErrorMessageLabel));
		String actualErrormsg=ErrorMessageLabel.getText();
		if(actualErrormsg.equals(expectedErrormsg))
		{
			return true;
		}
		else
		{
			return false;
		}
		
		
	}

}
