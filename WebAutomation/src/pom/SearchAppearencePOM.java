package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import generics.BasePage;

public class SearchAppearencePOM extends BasePage
{
	
	public SearchAppearencePOM(WebDriver driver) {
		super(driver);
	}
		
	public boolean verifysearchAppearencelinkpage(String expected)
	{
		wait.until(ExpectedConditions.titleContains(expected));
		String actual=driver.getTitle();
		if(actual.equals(expected))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

}
