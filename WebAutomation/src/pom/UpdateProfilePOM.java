package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import generics.BasePage;

public class UpdateProfilePOM extends BasePage
{

	public UpdateProfilePOM(WebDriver driver) 
	{
		super(driver);
	}
	
	public boolean verifyUpdateProfilePage(String expected)
	{
		wait.until(ExpectedConditions.titleContains(expected));
		String actualtitle=driver.getTitle();
		if(actualtitle.equals(expected))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
}
